//
//  QRView.swift
//  flutter_qr
//
//  Created by Julius Canute on 21/12/18.
//

import Foundation
import MTBBarcodeScanner
import ScanditBarcodeCapture

public class QRView:NSObject,FlutterPlatformView,BarcodeCaptureListener {
    @IBOutlet var previewView: UIView!
    var scanner: MTBBarcodeScanner?
    var registrar: FlutterPluginRegistrar
    var channel: FlutterMethodChannel
    private var licenseKey: String!
    private var symbologies: [Symbology]!
    private var context: DataCaptureContext
    var captureView: DataCaptureView
    var barcodeCapture: BarcodeCapture

    public init(withFrame frame: CGRect, withRegistrar registrar: FlutterPluginRegistrar, withId id: Int64){
        self.registrar = registrar
        self.licenseKey = ""
        self.symbologies = []

        self.context = DataCaptureContext(licenseKey: "Af2eZFdcSMrXI1dqADYfczUd1ZiIEBgRnFsCetxKhYalVgDFME705kR7A0E5YdcPtj2caWlEmVYSXngSjWbhI4tkqPXXQiAT1G1JJNJNVQGDf5+5rRvknk04jTekK7QUQg+CD3xfgtgd2s4rleSOXZsIX34pna/hJ163sXEEEcP9NW6dX92w2/HMj/r8zFKqk8Mfv+v/8XoXCixulKFYFv8n2cHAIsG9VG8myn+iPM16OyIBxo0h9v4sEyQzQz24nQlK5SWqLP5Ov7wmdwc0qmxqhT3l41YhU8ZnhoL4vRWaMtQnCA4SWtQaf4qGG6z2d9cOyHJDxsiySXdLaORO3hwjhOzRs2mVY0N4yeGcBK1bHDCU8fdiIRaNvR8NnS2wq36rDhJ/LhGfQAeb0pG2Ly1a/NGOW6iTwwS6jG/jH/OKCmBCzjghs1a18Mu4OQ6NfmM0wOiHMEHviKi5TV/ErWjikZOZCM9r0tZGX1HqbucLtqXTgO1zdT5rj1haeEG9lHVOdG+FMLUiSQpvb0HQhlF/+mPdov0gPA/fL5VWvrHKlN1k09bM9+dHXL6AXSxw4rL4bpVTAWFISkLYWSgSmeiVcEOjVcIgjjrNhwUXXIelUngFmmWfpN/fQmIzEgMYSum2Hsu7jMoxoW5Qwx771Q5hgwzudWaIu1OqlKQGgPyOMI1sukMjb7B98B+UFdD4AhgIQRcAwOBKVxL4YvdIAVn5Nba+raOHzbBtM7c+LHujAWbypsIZ7rNXy5g6aMb0K5xvoLLkPE0X9G0TxCbsKG7kin783WY8AXWJtDYit3SrBA6k")

        let settings = BarcodeCaptureSettings()
        settings.set(symbology: .code128, enabled: true)
        settings.set(symbology: .code39, enabled: true)
        settings.set(symbology: .qr, enabled: true)
        settings.set(symbology: .ean8, enabled: true)
        settings.set(symbology: .upce, enabled: true)
        settings.set(symbology: .ean13UPCA, enabled: true)
        barcodeCapture = BarcodeCapture(context: self.context, settings: settings)

        let cameraSettings = BarcodeCapture.recommendedCameraSettings
        let camera = Camera.default
        camera?.apply(cameraSettings)
        camera?.switch(toDesiredState: .on)
        context.setFrameSource(camera)

        captureView = DataCaptureView(context: self.context, frame: frame)
        //captureView.dataCaptureContext = context

        channel = FlutterMethodChannel(name: "net.touchcapture.qr.flutterqr/qrview_\(id)", binaryMessenger: registrar.messenger())
    }
    
    func isCameraAvailable(success: Bool) -> Void {
        if success {
            do {
                try scanner?.startScanning(resultBlock: { codes in
                    if let codes = codes {
                        for code in codes {
                            guard let stringValue = code.stringValue else { continue }
                            self.channel.invokeMethod("onRecognizeQR", arguments: stringValue)
                        }
                    }
                })
            } catch {
                NSLog("Unable to start scanning")
            }
        } else {
        }
    }

    public func barcodeCapture(_ barcodeCapture: BarcodeCapture, didScanIn session: BarcodeCaptureSession, frameData: FrameData) {
            //let recognizedBarcodes = session.newlyRecognizedBarcodes
            //for barcode in recognizedBarcodes {
                // Do something with the barcode.
            //}
    }
    
    public func view() -> UIView {
        channel.setMethodCallHandler({
            [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
            switch(call.method){
                case "setDimensions": break
                //    var arguments = call.arguments as! Dictionary<String, Double>
                //    self?.setDimensions(width: arguments["width"] ?? 0,height: arguments["height"] ?? 0)
                case "flipCamera":
                    self?.flipCamera()
                case "toggleFlash":
                    self?.toggleFlash()
                case "pauseCamera":
                    self?.pauseCamera()
                case "resumeCamera":
                    self?.resumeCamera()
                default:
                    result(FlutterMethodNotImplemented)
                    return
            }
        })


                barcodeCapture.addListener(self)

        return captureView
    }
    
    func setDimensions(width: Double, height: Double) -> Void {
        //captureView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
       //captureView.frame = CGRect(x: 0, y: 0, width: width, height: height)
       //scanner = MTBBarcodeScanner(previewView: previewView)
       //MTBBarcodeScanner.requestCameraPermission(success: isCameraAvailable)
    }
    
    func flipCamera(){
        if let sc: MTBBarcodeScanner = scanner {
            if sc.hasOppositeCamera() {
                sc.flipCamera()
            }
        }
    }
    
    func toggleFlash(){
        if let sc: MTBBarcodeScanner = scanner {
            if sc.hasTorch() {
                sc.toggleTorch()
            }
        }
    }
    
    func pauseCamera() {
        if let sc: MTBBarcodeScanner = scanner {
            if sc.isScanning() {
                sc.freezeCapture()
            }
        }
    }
    
    func resumeCamera() {
        if let sc: MTBBarcodeScanner = scanner {
            if !sc.isScanning() {
                sc.unfreezeCapture()
            }
        }
    }
}
